package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        Jogador start = new Jogador();
        start.comecarJogo();

        ArrayList<Integer> numeros = start.getNumeros();
        Impressao impressao = new Impressao(numeros);

        for(int i=1; i <= 3; i++){
            System.out.println("Jogo numero " + i);
            impressao.exibirResultado();
            System.out.println("*************************");
        }

    }
}
