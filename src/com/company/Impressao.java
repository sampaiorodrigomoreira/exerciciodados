package com.company;

import java.util.ArrayList;

public class Impressao {

    private ArrayList<Integer> numerosSorteados;

    public Impressao(ArrayList sorteados) {
        numerosSorteados = sorteados;
    }

    public void exibirResultado(){

        int soma = 0;
        for(Integer i:  numerosSorteados){
            soma += i;
            System.out.println("Numeros sorteados: " + i);
        }
        System.out.println("A soma dos numeros sorteados é: " + soma);

    }

}
